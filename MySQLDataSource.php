<?php

namespace zeageorge\MySQLDataSource;

use PDO;
use Exception;

/**
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class MySQLDataSource {

  public $pdo = null;
  public $host = 'localhost';
  public $port = 3306;
  public $db_name = '';
  public $username = '';
  public $password = '';
  public $table_prefix = '';
  public $charset = 'utf8';
  public $quote_character = "`";
  public $connection_options = [
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"
  ];
  public $connection_attributes = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
    PDO::ATTR_EMULATE_PREPARES => false,
  ];

  public function __construct(array $params = []) {
    $this->create_properties($params);
  }

  private function create_properties(array $params = []) {
    $default_params = [
      'pdo' => $this->pdo,
      'host' => $this->host,
      'port' => $this->port,
      'db_name' => $this->db_name,
      'username' => $this->username,
      'password' => $this->password,
      'table_prefix' => $this->table_prefix,
      'charset' => $this->charset,
      'quote_character' => $this->quote_character,
      'connection_options' => $this->connection_options,
      'connection_attributes' => $this->connection_attributes,
      'die_on_error' => true,
      'die_message' => "\n<br>Internal Database Error\n<br>",
      'http_header' => "HTTP/1.0 500 Internal Server Error",
      'http_status_code' => 500,
      'dev' => false,
      'logger' => null,
    ];
    foreach (array_merge($default_params, array_intersect_key($params, $default_params)) as $key => $value) {
      $this->$key = $value;
    }
  }

  /**
   * execQuery
   *
   * @param string $sql_query An SQL query string
   * @param array $bind_params Parameters to bind
   * @param int $fetch_mode A PDO Fetch mode PDO::FETCH_*. Default is PDO::FETCH_OBJ
   * @return array By default an array of objects, but depends on $fetchMode
   */
  public function execQuery(array $params = []) {
    $default_params = [
      'query' => '',
      'fetch_mode' => PDO::FETCH_OBJ,
      'bind_params' => [],
    ];
    $p = (object) array_merge($default_params, array_intersect_key($params, $default_params));

    try {
      $sth = $this->pdo->prepare($this->getPrefixedTableName($p->query));
      foreach ($p->bind_params as $key => $value) {
        $sth->bindValue("$key", $value, $this->getPdoType(\gettype($value)));
      }

      $r = $sth->execute();
      if ($r === false) {
        throw new Exception("Execution of prepared statement failed");
      }

      if (strpos(strtolower($p->query), 'select') === 0 ||
        strpos(strtolower($p->query), 'desc') === 0) {
        $re = $sth->fetchAll($p->fetch_mode);
        if ($re === false) {
          throw new Exception("Couldn't fetch results");
        }
        return $re;
      }

      return $r;
    } catch (Exception $exc) {
      $this->close();
      !$this->logger ?: $this->logger->error($exc);
      if ($this->die_on_error) {
        header($this->http_header, true, $this->http_status_code);
        if ($this->dev) {
          echo '<pre>';
          var_dump($exc);
          echo '</pre><br>';
        }
        die($this->die_message);
      }
      throw $exc;
    } finally {
      
    }
  }

  /**
   *
   * @param string $where
   *     $sqlQuery = "SELECT COUNT(*) as c FROM `$tableName` WHERE $where;";
   *
   * @param array $bind_params
   *    [
   *        ':p1'=>'value',
   *        ':p2'=>'value',
   *        ':pN'=>'value',
   *     ]
   * @return int
   */
  public function count(array $params = []) {
    $default_params = [
      'table_name' => '',
      'where' => null,
      'bind_params' => [],
    ];
    $p = (object) array_merge($default_params, array_intersect_key($params, $default_params));

    $sql_query = "SELECT COUNT(*) as c FROM " . $this->quoteTableName($this->getPrefixedTableName($p->table_name)) . ($p->where != null ? " WHERE $p->where;" : ";");
    try {
      $sth = $this->pdo->prepare($this->getPrefixedTableName($sql_query));
      foreach ($p->bind_params as $key => $value) {
        $sth->bindValue("$key", $value, $this->getPdoType(\gettype($value)));
      }

      if (!$sth->execute()) {
        throw new Exception("Execution of prepared statement failed");
      }

      $re = $sth->fetchAll(PDO::FETCH_ASSOC);

      if ($re === false) {
        throw new Exception("Couldn't fetch results");
      }

      return $re[0]['c'];
    } catch (Exception $exc) {
      $this->close();
      !$this->logger ?: $this->logger->error($exc);
      if ($this->die_on_error) {
        header($this->http_header, true, $this->http_status_code);
        if ($this->dev) {
          echo '<pre>';
          var_dump($exc);
          echo '</pre><br>';
        }
        die($this->die_message);
      }
      throw $exc;
    } finally {
      
    }
  }

  /**
   * insert one record into a table
   *
   * @param array $record An associative array:
   * [
   *      'tableColumnName1' => 'value',
   *      'tableColumnName2' => 'value',
   *      'tableColumnNameN' => 'value',
   * ]
   * @return integer|null Returns the id of the inserted row
   */
  public function insert(array $params = []) {
    $default_params = [
      'table_name' => '',
      'record' => null,
    ];
    $p = (object) array_merge($default_params, array_intersect_key($params, $default_params));
    try {
      if (!is_array($p->record)) {
        throw new Exception('Record to insert must be an array');
      }

      \ksort($p->record);
      $field_names = \implode('`, `', \array_keys($p->record));
      $field_values = ':' . \implode(', :', \array_keys($p->record));
      $sql_query = "INSERT INTO " . $this->quoteTableName($this->getPrefixedTableName($p->table_name)) . " (`$field_names`) VALUES ($field_values)";

      $sth = $this->pdo->prepare($this->getPrefixedTableName($sql_query));
      foreach ($p->record as $key => $value) {
        $sth->bindValue(":$key", $value, $this->getPdoType(\gettype($value)));
      }

      if (!$sth->execute()) {
        throw new Exception("Execution of prepared statement failed");
      }

      $last_insert_id = $this->pdo->lastInsertId();

      return $last_insert_id;
    } catch (Exception $exc) {
      $this->close();
      !$this->logger ?: $this->logger->error($exc);
      if ($this->die_on_error) {
        header($this->http_header, true, $this->http_status_code);
        if ($this->dev) {
          echo '<pre>';
          var_dump($exc);
          echo '</pre><br>';
        }
        die($this->die_message);
      }
      throw $exc;
    } finally {
      
    }
    return null;
  }

  /**
   * update
   *
   * @param string $table_name A name of table to insert into
   * @param array $data_to_update An associative array
   * [
   *      'tableColumnName1' => 'value',
   *      'tableColumnName2' => 'value',
   *      'tableColumnNameN' => 'value',
   * ]
   * @param string $where the WHERE query part AFTER the WHERE word
   *  eg. $sqlQuery = "UPDATE `" . $tableName . "` SET $dataToUpdate WHERE " . $where . ";";
   *    so the last part is used for the $where
   * @param array $bind_params_for_where_part
   * [
   *  ':id1' => 'someValue1',
   *  ':id2' => 'someValue2',
   *  ':idN' => 'someValueN',
   * ]
   * @return integer Returns the number of affected rows
   */
  public function update(array $params = []) {
    $default_params = [
      'table_name' => '',
      'data_to_update' => [],
      'where' => null,
      'bind_params_for_where_part' => [],
    ];
    $p = (object) array_merge($default_params, array_intersect_key($params, $default_params));

    try {
      if (!is_array($p->data_to_update)) {
        throw new Exception('data_to_update must be an array');
      }

      \ksort($p->data_to_update);
      $field_details = '';
      foreach ($p->data_to_update as $key => $value) {
        $field_details .= $this->quoteColumnName($key) . "=:$key,";
      }
      $field_details = \rtrim($field_details, ', ');
      $sql_query = "UPDATE " . $this->quoteTableName($this->getPrefixedTableName($p->table_name)) . " SET $field_details" . ($p->where != null ? " WHERE $p->where;" : ";");
      $sth = $this->pdo->prepare($sql_query);
      foreach ($p->data_to_update as $key => $value) {
        $sth->bindValue(":$key", $value, $this->getPdoType(\gettype($value)));
      }
      foreach ($p->bind_params_for_where_part as $key => $value) {
        $sth->bindValue("$key", $value, $this->getPdoType(\gettype($value)));
      }

      return $sth->execute();
    } catch (Exception $exc) {
      $this->close();
      !$this->logger ?: $this->logger->error($exc);
      if ($this->die_on_error) {
        header($this->http_header, true, $this->http_status_code);
        if ($this->dev) {
          echo '<pre>';
          var_dump($exc);
          echo '</pre><br>';
        }
        die($this->die_message);
      }
      throw $exc;
    } finally {
      
    }
  }

  /**
   * delete
   *
   * @param string $table_name
   * @param string $where the WHERE query part AFTER the WHERE word
   * @param array $bind_params
   *    [
   *        ':p1' => 'value',
   *        ':p2' => 'value',
   *        ':pN' => 'value',
   *     ]
   *  eg. $sqlQuery = "DELETE FROM `" . $tableName . "` WHERE " . $where . ";";
   * @return integer the number of affected rows
   */
  public function delete(array $params = []) {
    $default_params = [
      'table_name' => '',
      'where' => null,
      'bind_params' => [],
    ];
    $p = (object) array_merge($default_params, array_intersect_key($params, $default_params));

    try {
      $sql_query = "DELETE FROM " . $this->quoteTableName($this->getPrefixedTableName($p->table_name)) . ($p->where != null ? " WHERE $p->where;" : ";");
      $sth = $this->pdo->prepare($sql_query);
      foreach ($p->bind_params as $key => $value) {
        $sth->bindValue("$key", $value, $this->getPdoType(\gettype($value)));
      }
      return $sth->execute();
    } catch (Exception $exc) {
      $this->close();
      !$this->logger ?: $this->logger->error($exc);
      if ($this->die_on_error) {
        header($this->http_header, true, $this->http_status_code);
        if ($this->dev) {
          echo '<pre>';
          var_dump($exc);
          echo '</pre><br>';
        }
        die($this->die_message);
      }
      throw $exc;
    } finally {
      
    }
  }

  public function select_like(array $params = []) {
    $default_params = [
      'table_name' => '',
      'columns' => [],
      'exclude_columns' => ['password', 'encryption_key'],
      'filters' => [],
      'sort' => [],
      'offset' => 0,
      'items_per_page' => 20
    ];
    $p = (object) array_merge($default_params, array_intersect_key($params, $default_params));

    $sql_query = "SELECT ";
    if (count($p->columns)) {
      foreach ($p->columns as $column_name) {
        $sql_query .= $this->quoteColumnName($column_name) . ', ';
      }
      $sql_query = substr($sql_query, 0, -2);
    } else {
      $sql_query .= "*";
    }
    $sql_query .= " FROM " . $this->quoteTableName($this->getPrefixedTableName($p->table_name));

    $bind_params = [];
    if (count($p->filters)) {
      $sql_query .= " WHERE ";
      foreach ($p->filters as $key => $value) {
        $sql_query .= "`{$key}` LIKE :{$key}_f AND ";
        $bind_params[":{$key}_f"] = "{$value}";
      }
      $sql_query = substr($sql_query, 0, -5);
    }

    // $key AND $value must be validated/checked (in controller) to avoid sql injection !!!
    if (count($p->sort)) {
      $sql_query .= " ORDER BY ";
      foreach ($p->sort as $key => $value) {
        $sql_query .= "`{$key}` {$value}, ";
      }
      $sql_query = substr($sql_query, 0, -2);
    }

    $sql_query .= " LIMIT :offset, :row_count;";
    $bind_params[":offset"] = $p->offset;
    $bind_params[":row_count"] = $p->items_per_page;

    $sql_query = $this->getPrefixedTableName($sql_query);
    $r = $this->execQuery([
      'query' => $sql_query,
      'bind_params' => $bind_params
    ]);

    if (count($p->exclude_columns)) {
      foreach ($r as $record) {
        foreach ($p->exclude_columns as $column_to_exclude) {
          unset($record->{$column_to_exclude});
        }
      }
    }

    return $r;
  }

  /**
   * insert multiple records into a table
   * Use of transactions! if any insertion fails then roll back all!
   *
   * @param string $table_name A name of table to insert into
   * @param array $data_to_insert An associative array:
   * [
   *      'field_names' => [' ', ' ', ' ', ' ', ' ', ],
   *      'values' => [
   *              [' ', ' ', ' ', ' ', ' ', ],
   *              [' ', ' ', ' ', ' ', ' ', ],
   *              [' ', ' ', ' ', ' ', ' ', ],
   *              [' ', ' ', ' ', ' ', ' ', ],
   *      ],
   * ]
   * @return boolean Returns TRUE on success or FALSE on failure
   * @see http://stackoverflow.com/questions/1176352/pdo-prepared-inserts-multiple-rows-in-single-query
   * @see http://stackoverflow.com/questions/19680494/insert-multiple-rows-with-pdo-prepared-statements
   * @see http://stackoverflow.com/questions/10060721/pdo-mysql-insert-multiple-rows-in-one-query
   * @see http://dev.mysql.com/doc/refman/5.7/en/load-data.html
   */
  public function insertMultiple(array $params = []) {
    $default_params = [
      'table_name' => '',
      'data_to_insert' => [
        'field_names' => [],
        'values' => []
      ],
    ];
    $p = (object) array_merge($default_params, array_intersect_key($params, $default_params));

    try {
      if (!is_array($p->data_to_insert)) {
        throw new Exception('data_to_insert must be an array');
      }

      $field_names = \implode('`, `', \array_values($p->data_to_insert['field_names']));
      $sql_query = "INSERT INTO " . $this->quoteTableName($this->getPrefixedTableName($p->table_name)) . " (`$field_names`) VALUES ";

      $i = 0;
      $field_values = '';
      foreach ($p->data_to_insert['values'] as $rec_value) {
        $field_values .= '(';
        foreach ($rec_value as $value) {
          $field_values .= ":v$i,";
          $i++;
        }
        $field_values = \rtrim($field_values, ', ') . '), ';
      }
      $sql_query .= \rtrim($field_values, ', ') . ";";
      $sth = $this->pdo->prepare($sql_query);

      $i = 0;
      foreach ($p->data_to_insert['values'] as $rec_value) {
        foreach ($rec_value as $value) {
          $sth->bindValue(':v' . $i++, $value, $this->getPdoType(\gettype($value)));
        }
      }

      $this->pdo->beginTransaction();
      $sth->execute();
      $this->pdo->commit();
      return true;
    } catch (Exception $exc) {
      $this->pdo->rollback();
      $this->close();
      !$this->logger ?: $this->logger->error($exc);
      if ($this->die_on_error) {
        header($this->http_header, true, $this->http_status_code);
        if ($this->dev) {
          echo '<pre>';
          var_dump($exc);
          echo '</pre><br>';
        }
        die($this->die_message);
      }
      throw $exc;
    } finally {
      
    }
  }

  /**
   * Quotes a string value for use in a query.
   *
   * @param string $str string to be quoted
   * @return string the properly quoted string
   * @see http://www.php.net/manual/en/function.PDO-quote.php
   */
  public function quoteValue($str) {
    if (\is_int($str) || \is_float($str)) {
      return $str;
    }

    $connection_already_opened = false;
    if ($this->pdo == null) {
      $this->open();
    } else {
      $connection_already_opened = true;
    }

    if (($value = $this->pdo->quote($str)) !== false) {
      if ($this->pdo != null && !$connection_already_opened) {
        $this->close();
      }
      return $value;
    } else { // the driver doesn' t support quote (e . g . oci)
      if ($this->pdo != null && !$connection_already_opened) {
        $this->close();
      }
      return "'" . \addcslashes(\str_replace("'", "''", $str), "\000\n\r\\\032") . "'";
    }
  }

  /**
   * Quotes a table name for use in a query.
   * If the table name contains schema prefix, the prefix will also be properly quoted.
   *
   * @param string $name table name
   * @return string the properly quoted table name
   */
  public function quoteTableName($name) {
    return $this->quoteIdentifier($name);
  }

  /**
   * Quotes a column name for use in a query.
   * If the column name contains prefix, the prefix will also be properly quoted.
   *
   * @param string $name column name
   * @return string the properly quoted column name
   */
  public function quoteColumnName($name) {
    return $this->quoteIdentifier($name);
  }

  /**
   * Quote a string that is used as an identifier
   * (table names, column names etc). This method can
   * also deal with dot-separated identifiers eg table.column
   *
   * @param string $identifier
   * @return string
   */
  public function quoteIdentifier($identifier) {
    $parts = \array_map([$this, 'quoteIdentifierPart'], \explode('.', $identifier));
    return \join('.', $parts);
  }

  /**
   * This method performs the actual quoting of a single
   * part of an identifier, using the identifier quote
   * character.
   *
   * @param string $part
   * @return string
   */
  public function quoteIdentifierPart($part) {
    return $part === '*' ? $part : "{$this->quote_character}{$part}{$this->quote_character}";
  }

  /**
   * Determines the PDO type for the specified PHP type.
   *
   * @param string $type The PHP type (obtained by gettype() call).
   * \gettype($var) returns:
   * Possible values for the returned string are:
   * "boolean"
   * "integer"
   * "double" (for historical reasons "double" is returned in case of
   *          a float, and not simply "float")
   * "string"
   * "array"
   * "object"
   * "resource"
   * "null"
   * "unknown type"
   * @return integer the corresponding PDO type
   */
  public function getPdoType($type) {
    static $map = [
      'boolean' => PDO::PARAM_BOOL,
      'bool' => PDO::PARAM_BOOL,
      'integer' => PDO::PARAM_INT,
      'int' => PDO::PARAM_INT,
      'resource' => PDO::PARAM_LOB,
      'null' => PDO::PARAM_NULL,
    ];
    return isset($map[$type]) ? $map[$type] : PDO::PARAM_STR;
  }

  public function open(array $params = []) {
    $default_params = [];
    $p = (object) array_merge($default_params, array_intersect_key($params, $default_params));

    if ($this->pdo != null) { // connection already opened
      return $this;
    }

    try {
      $dsn = "mysql:host={$this->host};port={$this->port};dbname={$this->db_name}";
      $this->pdo = new PDO($dsn, $this->username, $this->password, $this->connection_options);

      foreach ($this->connection_attributes as $key => $value) {
        $this->pdo->setAttribute($key, $value);
      }

      if (!empty($this->charset)) {
        $this->pdo->exec("SET CHARACTER SET " . $this->pdo->quote($this->charset));
      }
    } catch (Exception $exc) {
      $this->close();
      !$this->logger ?: $this->logger->error($exc);
      if ($this->die_on_error) {
        header($this->http_header, true, $this->http_status_code);
        if ($this->dev) {
          echo '<pre>';
          var_dump($exc);
          echo '</pre><br>';
        }
        die($this->die_message);
      }
      throw $exc;
    }

    return $this;
  }

  public function close() {
    $this->pdo = null;
    return $this;
  }

  public function getPrefixedTableName($table_name) {
    return \str_replace("#__", $this->table_prefix, $table_name);
  }

}
