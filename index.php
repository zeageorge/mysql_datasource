<?php
require 'vendor/autoload.php';

use zeageorge\MySQLDataSource\MySQLDataSource;
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>MySQLDataSource test</title>
  </head>
  <body>
    <?php
    $db = new MySQLDataSource([
      'host' => 'localhost',
      'port' => 3306,
      'db_name' => 'pfp',
      'username' => 'pfp_sys',
      'password' => 'secret',
      'table_prefix' => '',
      'charset' => 'utf8',
      'quote_character' => "`",
      'connection_options' => [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'",
      ],
      'connection_attributes' => [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_EMULATE_PREPARES => false,
      ],
      'die_on_error' => true,
      'die_message' => "\n<br>DB Error\n<br>",
      'http_header' => $_SERVER["SERVER_PROTOCOL"] . " 500 Internal Server Error",
      'http_status_code' => 500,
    ]);

    $r = $db->open()->select_like([
      'table_name' => "#__users",
      'columns' => ['#__users.email_address', 'password'],
      'filters' => ['email_address' => '%.com'],
      'sort' => ['created_at' => 'asc'],
    ]);
    $db->close();

    echo "<pre>";
    var_dump($r);
    echo "</pre>";
    ?>
  </body>
</html>
